package io

import (
	"log"
	"os"
	"os/exec"
)

//FUNÇÃO UNEXPORTED (PRIVADA)
func getHelloWrl(param string) {
	cmd := exec.Command("echo", param)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()

	if err != nil {
		log.Println(err.Error())
		log.Fatal("O sistema foi encerrado")
	}
}
